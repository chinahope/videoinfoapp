package com.lwm.videoinfoapp.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VideoListResponse2 implements Serializable {
    public Paging paging;
    public List<Data> data;

    public static class Paging{
        public String next;
    }
    public static class Data{
        public String title;
        public String excerpt;
        public String image_url;
        public int comment_count;
        public int voteup_count;
        public int liked_count;
        public Author author;
        public Video video;
    }
    public static class Video{
        public String duration;
        public String play_auth_token;
        public PlayList playlist;
    }
    public static class PlayList{
        public Category hd;
        public Category ld;
        public Category sd;
    }
    public static class Category{
        public String play_url;
        public String duration;
    }

    public static class Author{
        public String name;
        public String avatar_url_template;
    }
}
