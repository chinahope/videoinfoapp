package com.lwm.videoinfoapp.activity;

import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lwm.videoinfoapp.R;
import com.lwm.videoinfoapp.entity.VideoEntity;
import com.lwm.videoinfoapp.util.Utils;

import xyz.doikki.videocontroller.StandardVideoController;
import xyz.doikki.videocontroller.component.CompleteView;
import xyz.doikki.videocontroller.component.ErrorView;
import xyz.doikki.videocontroller.component.GestureView;
import xyz.doikki.videocontroller.component.PrepareView;
import xyz.doikki.videocontroller.component.TitleView;
import xyz.doikki.videocontroller.component.VodControlView;
import xyz.doikki.videoplayer.player.VideoView;

public class VideoPlayActivity extends BaseActivity {
    private VideoEntity mVideoEntity;
    private ViewGroup mContainerView;
    private PrepareView mPrepareView;
    // 视频播放
    protected VideoView mVideoView;
    protected StandardVideoController mController;
    protected ErrorView mErrorView;
    protected CompleteView mCompleteView;
    protected TitleView mTitleView;

    @Override
    protected int initLayout() {
        return R.layout.activity_video_play;
    }

    @Override
    protected void initView() {
        mContainerView = findViewById(R.id.container_view);
        mPrepareView = findViewById(R.id.prepare_view);
        initVideoView();
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            mVideoEntity = (VideoEntity) intent.getSerializableExtra("entity");
        }
        if (mVideoEntity != null) {
            startPlay();
        } else {
            Toast.makeText(this, "video 数据异常", Toast.LENGTH_SHORT).show();
        }
    }

    // 初始化视频播放器
    protected void initVideoView() {
        mVideoView = findViewById(R.id.video_view);
        mVideoView.setOnStateChangeListener(new xyz.doikki.videoplayer.player.VideoView.SimpleOnStateChangeListener() {
            @Override
            public void onPlayStateChanged(int playState) {
                //监听VideoViewManager释放，重置状态
                if (playState == xyz.doikki.videoplayer.player.VideoView.STATE_IDLE) {
                    Utils.removeViewFormParent(mVideoView);
//                    mLastPos = mCurPos;
//                    mCurPos = -1;
                }
            }
        });
        mController = new StandardVideoController(this);
        mErrorView = new ErrorView(this);
        mController.addControlComponent(mErrorView);
        mCompleteView = new CompleteView(this);
        mController.addControlComponent(mCompleteView);
        mTitleView = new TitleView(this);
        mController.addControlComponent(mTitleView);
        mController.addControlComponent(new VodControlView(this));
        mController.addControlComponent(new GestureView(this));
        mController.setEnableOrientation(true);
        mVideoView.setVideoController(mController);
    }

    /**
     * 开始播放
     */
    protected void startPlay() {
        VideoEntity videoEntity = mVideoEntity;
        //边播边存
//        String proxyUrl = ProxyVideoCacheManager.getProxy(getActivity()).getProxyUrl(videoBean.getUrl());
//        mVideoView.setUrl(proxyUrl);

        mVideoView.setUrl(videoEntity.getPlayurl());
        mTitleView.setTitle(videoEntity.getVtitle());
        //把列表中预置的PrepareView添加到控制器中，注意isDissociate此处只能为true, 请点进去看isDissociate的解释
        mController.addControlComponent(mPrepareView, true);
        mVideoView.start();
    }

    public static void launchActivity(Context context, VideoEntity videoEntity) {
        Intent intent = new Intent(context, VideoPlayActivity.class);
        intent.putExtra("entity", videoEntity);
        context.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoView.release();
    }
}
