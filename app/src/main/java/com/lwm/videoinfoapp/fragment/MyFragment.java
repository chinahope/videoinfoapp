package com.lwm.videoinfoapp.fragment;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.lwm.videoinfoapp.R;

import butterknife.BindView;
import butterknife.OnClick;

public class MyFragment extends BaseFragment {

    @BindView(R.id.img_header)
    ImageView imgHeader;

    public MyFragment() {
    }

    public static MyFragment newInstance() {
        MyFragment fragment = new MyFragment();
        return fragment;
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_my;
    }

    @Override
    protected void initView() {
        Switch preloadSwitch = mRootView.findViewById(R.id.sw_proload);
        preloadSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity(), "开启预加载", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "关闭预加载", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.img_header, R.id.rl_clear, R.id.rl_preload, R.id.rl_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_clear: // 清洗缓存
                Toast.makeText(getActivity(), "已清理", Toast.LENGTH_SHORT).show();
                break;
            case R.id.rl_preload: // 预加载
                break;
            case R.id.rl_private: // 隐私
                break;
            case R.id.rl_version: // 版本信息
                break;
        }
    }
}